| Key Type | Fingerprint                                           |
|:---------|:------------------------------------------------------|
|RSA       | `SHA256:8YfGsVKsM9cdmngIJjD7HmDRF3OA1v6rc/6Sa7QnsHw`  |
|RSA       | `MD5:59:67:38:7c:91:a5:36:1f:d5:5c:3c:b8:99:e5:4d:62` |
|ED25519   | `SHA256:pdnv4nvxq3UWMqAosme0vvlqsqxqXnjbYHkrcqYjdsg`  |
|ED25519   | `MD5:31:15:37:f6:49:8b:80:18:2f:b0:d1:d1:d7:70:53:d4` |
|ECDSA     | `SHA256:0GrEjfFXz1mtaCYj2oeTqKgS1E6/F7ZaYBm0BUHo2Y0`  |
|ECDSA     | `MD5:a4:5e:41:96:35:60:54:5c:9f:43:a3:1d:e8:ec:10:ae` |
