# ZIH HPC Documentation

This is the documentation of the HPC systems and services provided at
[TU Dresden/ZIH](https://tu-dresden.de/zih/).

This documentation will be continuously updated, since we try
to incorporate more information with increasing experience and with every question you ask us.

If the provided HPC systems and services helped to advance your research, please cite us. Why this
is important and acknowledgment examples can be found in the section
[Acknowledgement](application/acknowledgement.md).

## Contribution

The HPC team invites you to take part in the improvement of these pages by correcting or adding
useful information. Your contributions are highly welcome!

The easiest way for you to contribute is to report issues via
the GitLab
[issue tracking system](https://gitlab.hrz.tu-chemnitz.de/zih/hpcsupport/hpc-compendium/-/issues).
Please check for any already existing issue before submitting your issue in order to avoid duplicate
issues.

Please also find out the other ways you could contribute in our
[guidelines how to contribute](contrib/howto_contribute.md).

!!! tip "Reminder"

    Non-documentation issues and requests need to be send to
    [hpc-support@tu-dresden.de](mailto:hpc-support@tu-dresden.de).

## News

* **2024-12-13** Regular user operation of the new
  [GPU cluster `Capella`](jobs_and_resources/capella.md) started
* **2024-11-18** The GPU cluster [`Capella`](jobs_and_resources/hardware_overview.md#capella) was
  ranked #51 in the [TOP500](https://top500.org/system/180298/), #3 in the German systems and #5 in
  the [GREEN500](https://top500.org/lists/green500/list/2024/11/) lists of the world's fastest
  computers in November 2024.
* **2024-11-04** Slides from the HPC Introduction tutorial in October
 2024 are available for [download now](misc/HPC-Introduction.pdf)

## Training and Courses

We offer a rich and colorful bouquet of courses from classical *HPC introduction*
([HPC introduction slides](misc/HPC-Introduction.pdf)) to various
*Performance Analysis* and *Machine Learning* trainings. Please refer to the page
[Training Offers](https://tu-dresden.de/zih/hochleistungsrechnen/nhr-training)
for a detailed overview of the courses and the respective dates at ZIH.

Furthermore, Center for Scalable Data Analytics and Artificial Intelligence
[ScaDS.AI](https://scads.ai) Dresden/Leipzig offers various trainings with HPC focus.
Current schedule and registration is available at the
[ScaDS.AI trainings page](https://scads.ai/transfer/teaching-and-training/).

## Support and Consultation

We offer regular
[public and personal consultation opportunities](support/support.md)
with HPC experts.
