# Application for HPC Resources

You will find comprehensive information regarding the application process on the webpage
[Project Application for using the HPC Systems](https://tu-dresden.de/zih/hochleistungsrechnen/zugang/projektantrag).

You will find in this section information about:

- [Terms of Use](terms_of_use.md)
- Manage members and their access to the project via
  [User Management for Project Leaders](project_management.md)
- [Acknowledgment in Publications](acknowledgement.md)

## NHR Center

Since 2021, HPC at universities has been restructured by the
[NHR network](https://www.nhr-verein.de/).
The network consists of nine centers which operate the systems and offer
a coordinated consulting service on the methodological competence of scientific HPC.
The aim is to provide scientists at German universities with computing capacity
for their research and to strengthen their skills in the efficient use of these resources.

In order to use the HPC systems installed at ZIH, it is necessary to apply for the resources.
The applicant (HPC project manager) is required to have a doctorate/PhD.
It is possible to apply for different
[project types](https://tu-dresden.de/zih/hochleistungsrechnen/zugang/projektantrag#section-1)
depending on the project demands.

However, the
[application workflow with JARDS](https://tu-dresden.de/zih/hochleistungsrechnen/zugang/projektantrag)
is identical for all types.

??? Info  "Who can apply?"

    Please note: Researchers with a doctorate from universities or universities of applied sciences
    in Germany are eligible to apply.

    We particularly invite researchers to apply with research projects related to our focus topics:

       * Life Sciences
       * Earth System Sciences
       * Methods for big data, data analysis and management
       * Machine Learning
       * Tiered storage architectures and I/O optimization
       * Performance and energy efficiency analysis and optimization
       * Research projects not related to our focus topics but from Universities within Saxony.

    We recommend **all other researchers** to review the focus topics of the remaining
    [NHR centers](https://www.nhr-verein.de/anwendungsunterstuetzung) and apply to the most suitable
    for your research topic. Working at the specialized NHR center related to your research topic
    provides you with the advantage of receiving topic specific support.
