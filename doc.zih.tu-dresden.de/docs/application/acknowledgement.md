# Acknowledgement

To provide you with modern and powerful HPC systems in future as well, we have to show that these
systems help to advance research. For that purpose we rely on your help. In most cases, the results
of your computations are used for presentations and publications, especially in peer-reviewed
magazines, journals, and conference proceedings. We kindly ask you to mention the HPC resource usage
in the acknowledgment section of all publications that are based on granted HPC resources of the
[NHR center at TU Dresden](https://tu-dresden.de/zih/hochleistungsrechnen/nhr-center). Examples:

!!! note "Standard case"

    The authors gratefully acknowledge the computing time made available to them on
    the high-performance computer <XY> at the NHR Center of TU Dresden. This center is jointly
    supported by the Federal Ministry of Education and Research and the state governments
    participating in the NHR (www.nhr-verein.de/unsere-partner).

!!! note "Two NHR centers"

    The authors gratefully acknowledge the computing time made available to them on
    the high-performance computers <XY> and <YZ> at the NHR Centers at TU Dresden and <YZ>.
    These centers are jointly supported by the Federal Ministry of Education and Research
    and the state governments participating in the NHR (www.nhr-verein.de/unsere-partner).

!!! note "German"

    Die Autoren bedanken sich für die ihnen zur Verfügung gestellte Rechenzeit auf
    dem Hochleistungsrechner <XY> am NHR-Zentrum der TU Dresden. Dieses wird gemeinsam durch
    das Bundesministerium für Bildung und Forschung und den am NHR beteiligten
    Landesregierungen (www.nhr-verein.de/unsere-partner) unterstützt.
