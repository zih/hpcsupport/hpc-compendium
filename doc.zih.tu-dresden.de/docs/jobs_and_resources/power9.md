# GPU Cluster Power9

## Overview

The multi-GPU cluster `Power9` was installed in 2018. Until the end of 2023, it was available as
partition `power` within the now decommissioned `Taurus` system. With the decommission of `Taurus`,
`Power9` has been re-engineered and is now a homogeneous, standalone cluster with own
[Slurm batch system](slurm.md) and own login nodes.

## Hardware Resources

The hardware specification of the cluster `Power9` is documented on the page
[HPC Resources](hardware_overview.md#power9).

We provide additional architectural information in the following.
The compute nodes of the cluster `Power9` are built on the base of
[Power9 architecture](https://www.ibm.com/it-infrastructure/power/power9) from IBM.
The system was created for AI challenges, analytics and working with data-intensive workloads and
accelerated databases.

The main feature of the nodes is the ability to work with the
[NVIDIA Tesla V100](https://www.nvidia.com/en-gb/data-center/tesla-v100/) GPU with **NV-Link**
support that allows a total bandwidth with up to 300 GB/s. Each node on the
cluster `Power9` has six Tesla V100 GPUs. You can find a detailed specification of the cluster in our
[Power9 documentation](../jobs_and_resources/hardware_overview.md#power9).

!!! note

    The cluster `Power9` is based on the PPC64 architecture, which means that the software built
    for x86_64 will not work on this cluster.

## Usage

### Containers

If you want to use containers on `Power9`, please refer to the page
[Singularity for Power9 Architecture](../software/singularity_power9.md).

### Power AI

There are tools provided by IBM, that work on cluster `Power9` and are related to AI tasks.
For more information see our [Power AI documentation](../software/power_ai.md).
