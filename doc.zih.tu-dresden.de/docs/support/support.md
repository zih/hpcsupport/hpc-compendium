# User Support

## Create a Ticket

The best way to ask for help send a message to
[hpc-support@tu-dresden.de](mailto:hpc-support@tu-dresden.de) with a
detailed description of your problem.

It should include:

- Who is reporting? (login name)
- Where have you seen the problem? (name of the HPC system and/or of the node)
- When has the issue occurred? Maybe, when did it work last?
- What exactly happened?

If possible include

- job ID,
- batch script,
- filesystem path,
- loaded modules and environment,
- output and error logs,
- steps to reproduce the error.

This email automatically opens a trouble ticket which will be tracked by the HPC team. Please
always keep the ticket number in the subject on your answers so that our system can keep track
on our communication.

For a new request, please simply send a new email (without any ticket number).

!!! hint "Please try to find an answer in this documentation first."

## Open Q&A Sessions

We invite you to join our public Q&A sessions with any questions you may have:

* Open Q&A session for users of the NHR@TUD HPC systems.

   * Every Monday from 1:30 - 2:00 pm, except on holidays.
   * See [ZIH calendar](https://tu-dresden.de/zih/das-department/termine/termine/nhr-trainings/qa-session-nhr-at-tud)
   for the next event and to download the event series to your calendar.

* Open AI Q&A session as a joint initiative for users of NHR and GCS centers.

   * Every Thursday from 2:00 - 3:00 pm.
   * See [NHR - AI on High Performance Computers](https://www.nhr-verein.de/ki-auf-hochleistungsrechnern)
   for further details.
   * Join the [zoom session](https://www.nhr-verein.de/en/ai-supercomputers).
