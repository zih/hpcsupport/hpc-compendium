#!/usr/bin/env bash

set -euo pipefail;

SCRIPT_PATH=$(realpath "${BASH_SOURCE[0]}")
BASE_DIR=$(dirname "${SCRIPT_PATH}")
PARENT_DIR=$(dirname "${BASE_DIR}")


function generate_actual_fingerprint_table_body_from_remote_host() {
  local the_host
  local keyscan_output
  the_host=$1
  keyscan_output=$(ssh-keyscan "$1" 2> /dev/null)
  ssh-keygen -lf - 2> /dev/null <<< "${keyscan_output}" \
    | sed -E 's;^[0-9]+ ([^ ]+) [^ ]+ \((RSA|ECDSA|ED25519)\)$;| \2 | `\1` |;'
  ssh-keygen -E md5 -lf - 2> /dev/null <<< "${keyscan_output}" \
    | sed -E 's;^[0-9]+ ([^ ]+) [^ ]+ \((RSA|ECDSA|ED25519)\)$;| \2 | `\1` |;'
}


function generate_actual_fingerprint_table_from_remote_host() {
  local the_host
  the_host=$1
  cat <<-EOF
| Key Type | Fingerprint                                           |
|:---------|:------------------------------------------------------|
EOF
  generate_actual_fingerprint_table_body_from_remote_host "${the_host}" | sort -r
}


return_code=0

while IFS=' ' read host expected_table_file; do
  echo "Checking fingerprints of ${host}"
  if diff -Naurw "${PARENT_DIR}/docs/access/${expected_table_file}" \
       <(generate_actual_fingerprint_table_from_remote_host "${host}"); then
    echo "... Fingerprints match expectation."
  else
    echo "... Expected fingerprints do not match obtained ones!"
    return_code=1
  fi
done < "${BASE_DIR}/host.config"
exit "${return_code}"
