#!/usr/bin/env bash

# Purpose:
#   Checks internal and external links for Markdown files (.md) of the repository
#   (i.e. HPC compendium). This script can check the URLs
#     1. in all Markdown files
#     2. Git-changed Markdown files
#     3. single Markdown file
#
#   For option 1, we make use of the html-proofer-plugin, which is configured in mkdocs.yml.
#   By default the link checker only inspects internal links. It will inspect internal and external
#   links, since we set the env. variable ENABLED_HTMLPROOFER_EXTERNAL_URLS to true in this script.
#
#   For option 2 and 3, we use the markdown-link-check script from
#   https://github.com/tcort/markdown-link-check, which can either be installed as local or global
#   module (nmp npm install [--save-dev|-g] markdown-link-check).
#
#   General remarks for future me:
#     - html-proofer-plugin is heavily customizable (ignore certain urls or pages, etc.)
#     - html-proofer-plugin is quite slow, since it requires to build the whole compendium
#       - html-proofer-plugin works on rendered html files
#     - html-proofer-plugin has no option to check a single page

set -eo pipefail

scriptpath=${BASH_SOURCE[0]}
basedir=`dirname "${scriptpath}"`
basedir=`dirname "${basedir}"`

usage() {
  cat <<-EOF
usage: $0 [file | -a | -c]

Options:
  <file>          Check links in the provided Markdown file
  -a; --all       Check links in all Markdown files
  -i; --internal  Check links in all Markdown files (internal links only)
  -c; --changed   Check links in all Git-changed files
EOF
}

mlc=markdown-link-check
if ! command -v ${mlc} &> /dev/null; then
  echo "INFO: ${mlc} not found in PATH (global module)"
  mlc=./node_modules/markdown-link-check/${mlc}
  if ! command -v ${mlc} &> /dev/null; then
    echo "INFO: ${mlc} not found (local module)"
    exit 1
  fi
fi

echo "mlc: ${mlc}"

LINK_CHECK_CONFIG="${basedir}/util/link-check-config.json"
if [[ ! -f "${LINK_CHECK_CONFIG}" ]]; then
  echo ${LINK_CHECK_CONFIG} does not exist
  exit 1
fi

branch="preview"
if [[ -n "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" ]]; then
    branch="origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
fi

function check_single_file(){
  theFile="$1"
  if [[ -e "${theFile}" ]]; then
    echo "Checking links in ${theFile}"
    if ! ${mlc} -q -c "${LINK_CHECK_CONFIG}" -p "${theFile}"; then
      return 1
    fi
  else
    echo "$1 not found"
  fi

  return 0
}

function check_files(){
  any_fails=false
  echo "Check files:"
  echo "${files}"
  echo ""
  for f in ${files}; do
    if ! check_single_file "${f}"; then
      any_fails=true
    fi
  done
  
  if [[ "${any_fails}" == true ]]; then
    exit 1
  fi
}

function check_changed_files(){
  files=$(git diff --name-only --diff-filter=d "$(git merge-base HEAD "${branch}")" -- '*.md' || true)
  check_files
}

# Make use of the html-proofer-plugin and build documentation
function check_all_files_hpp() {
  export ENABLED_HTMLPROOFER=true
  # Switch for wikiscript
  if [[ -d "doc.zih.tu-dresden.de" ]]; then
    cd doc.zih.tu-dresden.de
  fi
  mkdocs build
  rm -rf public
}

if [[ $# -ge 1 ]]; then
  case $1 in
  -h | help | -help | --help)
    usage
    exit 0
  ;;
  -a | --all)
    export ENABLED_HTMLPROOFER_EXTERNAL_URLS=true
    echo "Info: Building documentation and checking internal and external links. Might take some time."
    check_all_files_hpp
  ;;
  -i | --internal)
    export ENABLED_HTMLPROOFER_EXTERNAL_URLS=false
    echo "Info: Building documentation and checking internal links. Might take some time."
    check_all_files_hpp
  ;;
  -c | --changed)
    echo "Info: Checking links in Git-changed files."
    check_changed_files
  ;;
  *)
    echo "Info: Checking links in file $1"
    check_single_file "$1"
  ;;
  esac
else
  echo -e "Remark: The script changed in Oct. 2024. You did not provide any option.
        If you intend to check the links of all modified files, please
        provide the option \"-c\"\n"
  usage
  exit 1
fi
