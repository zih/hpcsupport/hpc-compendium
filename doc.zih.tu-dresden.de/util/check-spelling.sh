#!/usr/bin/env bash

set -euo pipefail

scriptpath=${BASH_SOURCE[0]}
basedir=`dirname "${scriptpath}"`
basedir=`dirname "${basedir}"`
wordlistfile=$(realpath ${basedir}/wordlist.aspell)
branch="origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME:-preview}"
aspellmode=
if aspell dump modes | grep -q markdown; then
  aspellmode="--mode=markdown"
fi

function usage() {
  cat <<-EOF
usage: $0 [file | -a]
If file is given, outputs all words of the file, that the spell checker cannot recognize.
If parameter -a (or --all) is given instead of the file, checks all Markdown files.
Otherwise, checks whether any changed file has more unrecognizable words than before the change.
If you are sure a word is correct, you can put it in ${wordlistfile}.
EOF
}

function get_aspell_output(){
  aspell -p "${wordlistfile}" --ignore 2 -l en_US ${aspellmode} list | sort -u
}

function get_number_of_aspell_output_lines(){
  get_aspell_output | wc -l
}

function is_wordlist_sorted(){
  #Unfortunately, sort depends on locale and docker does not provide much.
  #Therefore, it uses bytewise comparison. We avoid problems with the command tr.
  if sed 1d "${wordlistfile}" | tr [:upper:] [:lower:] | sort -C; then
    return 1
  fi
  return 0
}

function should_skip_file(){
  local result
  result="$(git check-attr check-spelling -- "$1")"
  test "${result#*: check-spelling: }" = "unset"
}

function check_all_files(){
  any_fails=false

  if is_wordlist_sorted; then
    echo "Unsorted wordlist in ${wordlistfile}"
    any_fails=true
  fi

  files=$(git ls-tree --full-tree -r --name-only HEAD ${basedir}/ | grep '\.md$')
  while read file; do
    if [[ "${file: -3}" == ".md" ]]; then
      if should_skip_file ${file}; then
        echo "Skip ${file}"
      else
        echo "Check ${file}"
        echo "-- File ${file}"
        if { cat "${file}" | get_aspell_output | tee /dev/fd/3 | grep -xq '.*'; } 3>&1; then
          any_fails=true
        fi
      fi
    fi
  done <<< "${files}"

  if [[ "${any_fails}" == true ]]; then
    return 1
  fi
  return 0
}

function is_mistake_count_increased_by_changes(){
  any_fails=false

  if is_wordlist_sorted; then
    echo "Unsorted wordlist in ${wordlistfile}"
    any_fails=true
  fi

  source_hash=`git merge-base HEAD "${branch}"`
  #Remove everything except lines beginning with --- or +++
  files=$(git diff ${source_hash} -- '*.md' \
    | sed -E -n 's#^(---|\+\+\+) ((/|./)[^[:space:]]+)$#\2#p')
  #Special case: when ${files} is empty, we need to prevent the failure at 'read newfile'
  if [[ -z "${files}" ]]; then
    return 0
  fi
  #echo "${files}"
  #echo "-------------------------"
  #Assume that we have pairs of lines (starting with --- and +++).
  while read oldfile; do
    read newfile
    if should_skip_file ${newfile:2}; then
      echo "Skip ${newfile}"
    else
      echo "Check ${newfile}"
      if [[ "${oldfile}" == "/dev/null" ]]; then
        #Added files should not introduce new spelling mistakes
        previous_count=0
      else
        previous_count=`git show "${source_hash}:${oldfile:2}" \
          | get_number_of_aspell_output_lines`
      fi
      if [[ "${newfile}" == "/dev/null" ]]; then
        #Deleted files do not contain any spelling mistakes
        current_count=0
      else
        #Remove the prefix "b/"
        newfile=${newfile:2}
        current_count=`cat "${newfile}" | get_number_of_aspell_output_lines`
      fi
      if [[ ${current_count} -gt ${previous_count} ]]; then
        cat <<-EOF
-- File ${newfile}
Change increases spelling mistakes from ${previous_count} to ${current_count}.
Misspelled/unknown words:
EOF
        cat "${newfile}" | get_aspell_output
        any_fails=true
      fi
    fi
  done <<< "${files}"

  if [[ "${any_fails}" == true ]]; then
    return 1
  fi
  return 0
}

if [[ $# -eq 1 ]]; then
  case $1 in
  help | -help | --help)
    usage
    exit
  ;;
  -a | --all)
    check_all_files
  ;;
  *)
    cat "$1" | get_aspell_output
  ;;
  esac
elif [[ $# -eq 0 ]]; then
  is_mistake_count_increased_by_changes
else
  usage
fi
