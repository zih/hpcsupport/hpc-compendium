#!/usr/bin/env bash

set -euo pipefail

scriptpath=${BASH_SOURCE[0]}
basedir=`dirname "${scriptpath}"`
basedir=`dirname "${basedir}"`
cd ${basedir}/tud_theme/javascripts
wget https://unpkg.com/mermaid@11.4.1/dist/mermaid.min.js
