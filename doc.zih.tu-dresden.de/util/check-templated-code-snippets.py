#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import sys


def escapeSomeSigns(someString):
    return someString.translate({i: '\\' + chr(i) for i in b'\\$()*[]'})


fileName = sys.argv[1]
print("FILE: " + fileName)
lines = []
NORMAL_MODE = 0
CODE_MODE = 1
readMode = NORMAL_MODE
# We need to avoid matches for "#include <iostream>", <your.email>@tu-dresden.de,
# "<Ctrl+D>", "<-" (typically in R) and "<594>" (VampirServer)
pattern = re.compile(r"(?<!#include )<(?!your.email|Ctrl\+)[^0-9 -][^<>']*>")
try:
    with open(fileName) as f:
        lineNumber = 1
        for line in f:
            if "```" in line:
                # toggle read mode if we find a line with ```, so that we know that we are in a code block or not
                readMode = CODE_MODE if readMode == NORMAL_MODE else NORMAL_MODE
            strippedLine = line.strip()
            # We want tuples with lineNumber, the line itself, whether it is a code line, whether it contains a template (e. g. <FILENAME>)
            lines.append((lineNumber, strippedLine, readMode, pattern.search(strippedLine) is not None))
            lineNumber += 1
except FileNotFoundError:
    print("  File not found, probably deleted")
# those tuples with the CODE_MODE as field 2 represent code lines
codeLines = [line for line in lines if line[2] == CODE_MODE]
# From the code lines which contain a template we take
# line number, the line, and a regular expression of the line with all templates replaced by '\S'
templatedLines = [(lineNumber, line, re.compile(pattern.sub(r"\\S*", escapeSomeSigns(line))))
                  for lineNumber, line, _, isTemplate in codeLines if isTemplate]
allPatternsFound = True
for templateLineNum, templateLine, templateRegex in templatedLines:
    # find code lines which have a higher line number than the templateLine,
    # contain no template themselves and match the pattern of the templateLine
    matchingCodeLines = (lineNumber > templateLineNum and not isTemplate and templateRegex.match(line) is not None
                         for lineNumber, line, _, isTemplate in codeLines)
    if not any(matchingCodeLines):
        allPatternsFound = False
        print("  Example for \"" + templateLine + "\" (Line " + str(templateLineNum) + ") missing")

if not allPatternsFound:
    sys.exit(1)
